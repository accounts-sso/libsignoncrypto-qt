include( ../../common-project-config.pri )
include( ../../common-vars.pri )

TEMPLATE = lib
TARGET = signoncrypto-qt

HEADERS = \
    common.h \
    debug.h \
    encryptor.h

INCLUDEPATH += . \
               $$TOP_SRC_DIR/include

QT += core 
QT -= gui

CONFIG += build_all \
          link_pkgconfig debug
          
QMAKE_CXXFLAGS += -fno-exceptions \
                  -fno-rtti

system(pkg-config --exists aegis-crypto) {
    PKGCONFIG += aegis-crypto
    DEFINES += HAVE_AEGIS_CRYPTO
    SOURCES += encryptor.cpp
} else {
    SOURCES += encryptor-stub.cpp
}

DEFINES += QT_NO_CAST_TO_ASCII QT_NO_CAST_FROM_ASCII
include( $$TOP_SRC_DIR/common-installs-config.pri )

headers.files = \ 
    common.h \
    Encryptor \
    encryptor.h

headers.path = $${INSTALL_PREFIX}/include/signoncrypto-qt/SignOnCrypto
INSTALLS += headers

pkgconfig.files = libsignoncrypto-qt.pc
pkgconfig.path = $${INSTALL_PREFIX}/lib/pkgconfig
INSTALLS += pkgconfig

/*
 * This file is part of signon-crypto
 *
 * Copyright (C) 2009-2010 Nokia Corporation.
 *
 * Contact: Alexander Akimov <ext-alexander.akimov@nokia.com>
 * Contact: Alberto Mardegan <alberto.mardegan@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

/*!
 * @copyright Copyright (C) 2011 Nokia Corporation.
 * @license LGPL
 */

#ifndef SIGNONCRYPTO_COMMON_H
#define SIGNONCRYPTO_COMMON_H

#if __GNUC__ >= 4
    #define SIGNONCRYPRO_EXPORT __attribute__ ((visibility("default")))
#endif

#ifndef SIGNONCRYPRO_EXPORT
    #define SIGNONCRYPRO_EXPORT
#endif

#define SIGNONC_ENCODED_BLOB_KEY "encodedBlob"
#define SIGNONC_BLOBS_NUMBER_KEY "blobsNumber"
#define SIGNONC_ENCODED_FAKEDATA_STRING "encodedFakeDataForAegis"
#define SIGNONC_SERIALIZATION_TYPE_KEY "serializationType"
#define SIGNONC_NONDESERIALIZED_DATA_KEY "nonDeserializedData"

#define SIGNONC_AEGIS_MIN_DATA_SIZE 16

#endif // LIBSIGNONCOMMON_H

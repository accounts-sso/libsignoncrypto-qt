/*
 * This file is part of signoncrypto
 *
 * Copyright (C) 2009-2010 Nokia Corporation.
 *
 * Contact: Alberto Mardegan <alberto.mardegan@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

/*!
 * @copyright Copyright (C) 2011 Nokia Corporation.
 * @license LGPL
 */

#include "debug.h"
#include "encryptor.h"

using namespace SignOnCrypto;

Encryptor::Encryptor()
{
    TRACE() << "Stub implmentation";
}

Encryptor::~Encryptor()
{
    TRACE();
}

bool Encryptor::isVariantMapEncrypted(const QVariantMap &data) const
{
    Q_UNUSED(data);
    return false;
}

QVariantMap Encryptor::encodeVariantMap(const QVariantMap &data, pid_t pid)
{
    Q_UNUSED(pid);
    return QVariantMap(data);
}

QVariantMap Encryptor::decodeVariantMap(const QVariantMap &data, pid_t pid)
{
    Q_UNUSED(pid);
    return QVariantMap(data);
}

QString Encryptor::encodeString(const QString &data, pid_t pid)
{
    Q_UNUSED(pid);
    return QString(data);
}

QString Encryptor::decodeString(const QString &data, pid_t pid)
{
    Q_UNUSED(pid);
    return QString(data);
}

Encryptor::Status Encryptor::status() const
{
    return Encryptor::Ok;
}


/*
 * This file is part of signoncrypto
 *
 * @copyright Copyright (C) 2009-2010 Nokia Corporation.
 *
 * Contact: Alberto Mardegan <alberto.mardegan@nokia.com>
 *
 * @license GNU Lesser General Public License Version 2.1
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

/*!
 * @copyright Copyright (C) 2011 Nokia Corporation.
 * @license LGPL
 */

#include "debug.h"
#include "encryptor.h"

#include <QDataStream>
#include <QListIterator>

#include <sys/creds.h>
#include <aegis_crypto.h>


#define MAX_BUFFER_INPUT_SIZE 4000
#define PRINT_DATA_AFTER_PROCESSING 1

#define TOKEN_MAXSIZE 256
namespace SignOnCrypto {

class EncryptorPrivate {
public:
    EncryptorPrivate();
    ~EncryptorPrivate();

    QByteArray fetchToken(const pid_t pid);

    QByteArray encryptData(const QByteArray &clearData,
                           const QByteArray &token);

    QByteArray decryptData(const QByteArray &cipherText,
                          const QByteArray &token);

    char *self_token;
    int self_token_len;
    Encryptor::Status status;
};
} //namespace

using namespace SignOnCrypto;

EncryptorPrivate::EncryptorPrivate()
{
    TRACE();

    self_token = NULL;
    self_token_len = 0;
}

EncryptorPrivate::~EncryptorPrivate()
{
    if (self_token)
        free(self_token);
    TRACE();
}

QByteArray EncryptorPrivate::fetchToken(const pid_t pid)
{
    /*
     * we cache token for our own process only
     * */
    TRACE() << pid << self_token_len;
    if (pid == 0 && self_token_len != 0)
        return QByteArray(self_token, self_token_len);
    TRACE();

    char token[TOKEN_MAXSIZE];
    memset(token, 0, TOKEN_MAXSIZE);
    creds_t ccreds = creds_gettask(pid);
    int res = creds_find(ccreds, "*sso-encryption-token", token, TOKEN_MAXSIZE);

    TRACE() << "creds_find = " << res;
    creds_free(ccreds);

    if (res == -1) {
        TRACE() << "No encryption credentials are specified: no need for encryption";
        return QByteArray();
    } else if (res >= TOKEN_MAXSIZE) {
        return QByteArray();
    }

    TRACE() << token;

    if (pid == 0 &&
        self_token_len == 0) {

        self_token = (char*)malloc(res);
        memcpy(self_token, token, res);
        self_token_len = res;
    }

    return QByteArray(token, res);
}

QByteArray EncryptorPrivate::encryptData(const QByteArray &clearData, const QByteArray &token)
{
    RAWDATA_PTR cipherData = 0;
    size_t cipherLength = 0;

    TRACE() << clearData;

    if (aegis_crypto_encrypt(clearData.data(),
                             clearData.length(),
                             token.constData(),
                             &cipherData,
                             &cipherLength) != aegis_crypto_ok) {

        aegis_crypto_free(cipherData);
        TRACE() << QString::fromLatin1("Failed to encrypt data: %1").
            arg(QLatin1String(aegis_crypto_last_error_str()));
        return QByteArray();
    }

    QByteArray encrypted((char *)cipherData, cipherLength);
    aegis_crypto_free(cipherData);

    if (PRINT_DATA_AFTER_PROCESSING) {
        QByteArray tmp(encrypted);
        TRACE() << "Encoded data: " << tmp.toBase64();
    }

    return encrypted;
}

QByteArray EncryptorPrivate::decryptData(const QByteArray &cipherData, const QByteArray &token)
{
    TRACE();
    RAWDATA_PTR clearData = NULL;
    size_t length = 0;

    if (aegis_crypto_decrypt(cipherData.data(),
                             cipherData.length(),
                             token.constData(),
                             &clearData,
                             &length) != aegis_crypto_ok)
    {
        aegis_crypto_free(clearData);
        TRACE() << QString::fromLatin1("Failed to decrypt data: %1.").
            arg(QLatin1String(aegis_crypto_last_error_str()));
        return QByteArray();
    }

    QByteArray decrypted((char *)clearData, length);
    aegis_crypto_free(clearData);

    if (PRINT_DATA_AFTER_PROCESSING) {
        TRACE() << "Decoded data: " << decrypted;
    }

    return decrypted;
}

const QString encodedBlobKey(QString::fromLatin1(SIGNONC_ENCODED_BLOB_KEY));
const QString blobsNumberKey(QString::fromLatin1(SIGNONC_BLOBS_NUMBER_KEY));
const QString serializationTypeKey(QString::fromLatin1(SIGNONC_SERIALIZATION_TYPE_KEY));
const QString nonDeserializedDataKey(QString::fromLatin1(SIGNONC_NONDESERIALIZED_DATA_KEY));

Encryptor::Encryptor()
{
    TRACE();
    priv = new EncryptorPrivate;

    if (!priv)
        qFatal("Cannot allocate memory for EncryptorPrivate");

    priv->status = Encryptor::Ok;
}

Encryptor::~Encryptor()
{
    TRACE();
    delete priv;
}

bool Encryptor::isVariantMapEncrypted(const QVariantMap &data) const
{
    return data.contains(blobsNumberKey);
}

QVariantMap Encryptor::encodeVariantMap(const QVariantMap &data, pid_t pid)
{
    priv->status = Encryptor::Ok;
    if (data.isEmpty())
        return QVariantMap();

    QByteArray securityToken = priv->fetchToken(pid);
    if (securityToken.isNull())
        return QVariantMap(data);

    QByteArray serializedData;
    QDataStream serializer(&serializedData, QIODevice::WriteOnly);
    serializer.setVersion(QDataStream::Qt_4_7);

    serializer << QVariant(data);
    if (serializedData.size() <= SIGNONC_AEGIS_MIN_DATA_SIZE)
        serializer << QVariant(QByteArray(SIGNONC_ENCODED_FAKEDATA_STRING));

    QVariantMap encodedVariantMap;
    QList<QByteArray> encodedData;
    int bytesProcessed = 0;

     do {
        int nonProcessedBytes = serializedData.size() - bytesProcessed;
        int bytesToEncode = (nonProcessedBytes < MAX_BUFFER_INPUT_SIZE ?
                nonProcessedBytes : MAX_BUFFER_INPUT_SIZE);

        QByteArray bufferToEncode(serializedData.constData() + bytesProcessed, bytesToEncode);
        QByteArray encodedBuffer(priv->encryptData(bufferToEncode, securityToken));

        if (encodedBuffer.isNull()) {
            priv->status = Encryptor::AegisCryptoError;
            return QVariantMap();
        }

        encodedData << encodedBuffer;
        bytesProcessed += bytesToEncode;
    } while (bytesProcessed < serializedData.size());

    encodedVariantMap.insert(serializationTypeKey,
                             (qint32)Encryptor::QtClientType);
    encodedVariantMap.insert(blobsNumberKey,
                             encodedData.size());
    for (int i = 0; i < encodedData.size(); i++)
        encodedVariantMap.insert(encodedBlobKey + QString::number(i),
                                 encodedData.at(i));
    return encodedVariantMap;
}

QVariantMap Encryptor::decodeVariantMap(const QVariantMap &data, pid_t pid)
{
    priv->status = Encryptor::Ok;

    if (!data.contains(blobsNumberKey)) {
        priv->status = Encryptor::Ok;
        return QVariantMap(data);
    }

    QByteArray securityToken = priv->fetchToken(pid);

    if (securityToken.isNull()) {
        priv->status = Encryptor::UnableToFetchToken;
        return QVariantMap();
    }

    QVariant serializationType = data.value(serializationTypeKey);
    int size = data.value(blobsNumberKey).toInt();

    QByteArray serializedData;
    for (int i = 0; i < size; i++) {
        QByteArray decodedBuffer(priv->decryptData(data.value(encodedBlobKey + QString::number(i)).toByteArray(),
                                                   securityToken));
        serializedData.append(decodedBuffer);
    }

    if (serializedData.isNull()) {
        priv->status = Encryptor::AegisCryptoError;
        return QVariantMap();
    }

    if (serializationType.toInt() != (qint32)Encryptor::QtClientType) {
        QVariantMap nonDeserializedResult;
        nonDeserializedResult.insert(nonDeserializedDataKey, serializedData);
        return nonDeserializedResult;
    }

    QDataStream deserializer(&serializedData, QIODevice::ReadOnly);
    QVariant decodedDataVa;
    deserializer >> decodedDataVa;

    if (deserializer.status() != QDataStream::Ok) {
        priv->status = Encryptor::CorruptedData;
        return QVariantMap();
    }

    return decodedDataVa.toMap();
}


/*
 * We can't specify a workaround for the case when string is longer than MAX_CRYPTO_INPUT_SIZE
 * */
QString Encryptor::encodeString(const QString &data, pid_t pid)
{
    priv->status = Encryptor::Ok;

    QByteArray securityToken = priv->fetchToken(pid);
    if (securityToken.isNull())
        return QString(data);

    if (data.isNull())
        return QString::fromLatin1("");

    QString stringToBeEncoded = QString::fromLatin1(SIGNONC_ENCODED_FAKEDATA_STRING) + data;
    QByteArray encodedData(priv->encryptData(stringToBeEncoded.toUtf8(), securityToken));

    if (encodedData.isNull()){
        priv->status = Encryptor::AegisCryptoError;
        return QString();
    }

    return QString::fromUtf8(encodedData.toBase64());
}

QString Encryptor::decodeString(const QString &data, pid_t pid)
{
    priv->status = Encryptor::Ok;

    QByteArray securityToken = priv->fetchToken(pid);
    if (securityToken.isNull())
        return QString(data);

    if (data.isEmpty())
        return QString();

    QByteArray encodedData(QByteArray::fromBase64(data.toUtf8()));
    QByteArray decodedData(priv->decryptData(encodedData, securityToken));

    if (decodedData.isNull()){
        priv->status = Encryptor::AegisCryptoError;
        return QString();
    }

    QString decodedString = QString::fromUtf8(decodedData.constData());
    QString fakeString = QString::fromLatin1(SIGNONC_ENCODED_FAKEDATA_STRING);

    return decodedString.right(decodedString.length() - fakeString.length());
}

Encryptor::Status Encryptor::status() const
{
    return priv->status;
}


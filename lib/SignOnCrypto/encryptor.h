/*
 * This file is part of signoncrypto-qt
 *
 * Copyright (C) 2009-2010 Nokia Corporation.
 *
 * Contact: Alberto Mardegan <alberto.mardegan@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

/*!
 * @copyright Copyright (C) 2011 Nokia Corporation.
 * @license LGPL
 */

#ifndef ENCRYPTOR_H
#define ENCRYPTOR_H

#include <sys/types.h>

#include <QByteArray>
#include <QMap>
#include <QString>
#include <QVariant>

#include "common.h"

namespace SignOnCrypto {

class EncryptorPrivate;

/*!
 * @class Encryptor
 * @headerfile SignOnCrypto/encryptor.h SignOnCrypto/Encryptor
 * @brief Provide encryption of strings and QVariantMaps.
 *
 * This class can be used to encrypt data going to and coming from the SignOn
 * daemon. It's not meant to be directly used by application developers.
 */
class Encryptor
{
public:
    /*!
     * Operation status.
     */
    enum Status {
        Ok,
        UnableToFetchToken,  /** Cannot fetch token required for encodeing/decoding */
        CorruptedData,       /** DataStream was successfully decoded but data in it is corrupted */
        AegisCryptoError,    /** Error happened during encryption/decryption */
        SerializationTypeIsNotSupported /* Client should deserialize data by its own method */
    };

    /** qt and glib versions of client use different serialization */
    enum SerializationType {
        QtClientType,
        GlibClientType,
        OtherType
    };

    /*!
     * Constructor.
     */
    Encryptor();
    virtual ~Encryptor();

    /*!
     * @param data A QVariantMap.
     * @return Whether the data is encrypted.
     */
    bool isVariantMapEncrypted(const QVariantMap &data) const;

    /*!
     * @param data The QVariantMap to encode.
     * @param pid The process ID of the process which owns the token which
     * will be used to encode the data.
     * @return The encoded data.
     */
    QVariantMap encodeVariantMap(const QVariantMap &data, pid_t pid = 0);

    /*!
     * @param data The QVariantMap to decode.
     * @param pid The process ID of the process which owns the token which
     * will be used to decode the data.
     * @return The decoded data.
     */
    QVariantMap decodeVariantMap(const QVariantMap &data, pid_t pid = 0);

    /*!
     * @param data The QString to encode.
     * @param pid The process ID of the process which owns the token which
     * will be used to encode the data.
     * @return The encoded string.
     */
    QString encodeString(const QString &data, pid_t pid = 0);

    /*!
     * @param data The QString to decode.
     * @param pid The process ID of the process which owns the token which
     * will be used to decode the data.
     * @return The decoded string.
     */
    QString decodeString(const QString &data, pid_t pid = 0);

    /*!
     * @return The status of the Encryptor.
     */
    Status status() const;

private:
    EncryptorPrivate *priv;
};
} //namespace

#endif //ENCRYPTOR_H

/*
 * This file is part of  signoncrypto-qt
 *
 * Copyright (C) 2009-2010 Nokia Corporation.
 *
 * Contact: Alberto Mardegan <alberto.mardegan@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "test.h"

#include <SignOnCrypto/Encryptor>
#include <QtTest/QtTest>
#include <SignOnCrypto/encryptor-glib.h>

using namespace SignOnCrypto;

const QString boolKey(QString::fromLatin1("booleanValue"));
const QString integerKey(QString::fromLatin1("integerValue"));
const QString byteArrayKey(QString::fromLatin1("byteArrayValue"));
const QString stringKey(QString::fromLatin1("stringValue"));

void Test::initTestCase()
{
}

void Test::cleanupTestCase()
{
}

void Test::sessionDataTest()
{
    Encryptor *coder = new Encryptor;
    QVERIFY(coder != NULL);

    bool boolParam = true;
    qint32 intParam = 111;
    QByteArray byteArrayParam("QByteArray parameter");
    QString stringParam(QString::fromLatin1("QString parameter"));

    QVariantMap originalData;
    originalData.insert(boolKey, boolParam);
    originalData.insert(integerKey, intParam);
    originalData.insert(byteArrayKey, byteArrayParam);
    originalData.insert(stringKey, stringParam);

    for (int i = 0; i < 4; i++)
    {
        QVariantMap encodedData(coder->encodeVariantMap(originalData, 0));
        QVERIFY(coder->status() == Encryptor::Ok);

        QVariantMap decodedData(coder->decodeVariantMap(encodedData, 0));
        QVERIFY(coder->status() == Encryptor::Ok);
        QVERIFY(decodedData == originalData);
    }

    originalData = QVariantMap();
    originalData.insert(QString::fromLatin1("p"), boolParam);

    for (int i = 0; i < 4; i++)
    {
        QVariantMap encodedData(coder->encodeVariantMap(originalData, 0));
        QVERIFY(coder->status() == Encryptor::Ok);

        QVariantMap decodedData(coder->decodeVariantMap(encodedData, 0));
        QVERIFY(coder->status() == Encryptor::Ok);
        QVERIFY(decodedData == originalData);
    }

    originalData = QVariantMap();
    char veryBigData[4096];
    memset(veryBigData, 0, 4096);

    originalData.insert(QString::fromLatin1("veryBigData1"), QByteArray(veryBigData, 4096));
    originalData.insert(QString::fromLatin1("veryBigData2"), QByteArray(veryBigData, 4096));

    for (int i = 0; i < 4; i++)
    {
        QVariantMap encodedData(coder->encodeVariantMap(originalData, 0));
        QVERIFY(coder->status() == Encryptor::Ok);

        QVariantMap decodedData(coder->decodeVariantMap(encodedData, 0));
        QVERIFY(coder->status() == Encryptor::Ok);
        QVERIFY(decodedData == originalData);
    }

    delete coder;
}

void Test::stringTest()
{
    Encryptor *coder = new Encryptor;

    QString orginalString(QString::fromLatin1(""));

    for (int i = 0; i < 4; i++)
    {
        QString encodedString(coder->encodeString(orginalString, 0));
        QVERIFY(coder->status() == Encryptor::Ok);

        QString decodedString(coder->decodeString(encodedString, 0));
        QVERIFY(coder->status() == Encryptor::Ok);
        QVERIFY(decodedString == orginalString);
    }

    orginalString = QString::fromLatin1("Hello Kitty!");

    for (int i = 0; i < 4; i++)
    {
        QString encodedString(coder->encodeString(orginalString, 0));
        QVERIFY(coder->status() == Encryptor::Ok);

        QString decodedString(coder->decodeString(encodedString, 0));
        QVERIFY(coder->status() == Encryptor::Ok);
        QVERIFY(decodedString == orginalString);
    }

    delete coder;
}

static void signon_free_gvalue(gpointer val)
{
    g_return_if_fail(G_IS_VALUE(val));
    GValue *value = (GValue *)val;
    g_value_unset(value);
    g_slice_free(GValue, value);
}

void Test::glibMapTest()
{
    g_type_init();

    GHashTable *map = g_hash_table_new_full(g_str_hash, g_str_equal,
                                            g_free, signon_free_gvalue);
    GValue *user_name = g_slice_new0(GValue);
    g_value_init(user_name, G_TYPE_STRING);
    g_value_set_static_string(user_name, "user_x");
    gchar *user_name_key = g_strdup("UserName");
    g_hash_table_insert(map, user_name_key, user_name);
    GHashTable *encrypted_map = g_hash_table_new_full(g_str_hash, g_str_equal,
                                                      g_free, signon_free_gvalue);
    int status = signon_encrypt_hash_table(map, encrypted_map, 0);
    QVERIFY(status == 0);
    GHashTable *decrypted_map = g_hash_table_new_full(g_str_hash, g_str_equal,
                                                      g_free, signon_free_gvalue);
    status = signon_decrypt_hash_table(encrypted_map, decrypted_map, 0);
    QVERIFY(status == 0);

    GValue *dec_user_name = (GValue *)g_hash_table_lookup(decrypted_map, user_name_key);
    QVERIFY(dec_user_name != NULL &&
            G_VALUE_TYPE(dec_user_name) == G_TYPE_STRING);
    QVERIFY(strcmp(g_value_get_string(dec_user_name),
                   g_value_get_string(user_name)) == 0);

    g_hash_table_destroy(decrypted_map);
    g_hash_table_destroy(encrypted_map);
    g_hash_table_destroy(map);
}

void Test::glibStringTest()
{
    g_type_init();

    const gchar *test_string = "test_string";
    gchar *encrypted_string = NULL;
    int status = signon_encrypt_string((gchar *)test_string, &encrypted_string, 0);
    QVERIFY(status == 0);

    gchar *decrypted_string = NULL;
    status = signon_decrypt_string(encrypted_string, &decrypted_string, 0);
    QVERIFY(status == 0);

    QVERIFY(strcmp(test_string, decrypted_string) == 0);

    g_free(encrypted_string);
    g_free(decrypted_string);
}

QTEST_APPLESS_MAIN(Test)

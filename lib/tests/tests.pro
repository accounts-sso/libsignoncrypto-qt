include( ../../common-project-config.pri )
include( ../../common-vars.pri )

QMAKE_EXTRA_TARGETS += check
check.depends = libsignoncrypto-qt-test
check.commands = ./libsignoncrypto-qt-test

TARGET = libsignoncrypto-qt-test

CONFIG += \
    qtestlib \
    qt

SOURCES += \
    test.cpp
HEADERS += \
    test.h

QT += core
QT -= gui

QMAKE_LIBDIR += $${TOP_SRC_DIR}/lib/SignOnCrypto
LIBS += -lsignoncrypto-qt

DEPENDPATH += $${INCLUDEPATH}

include( ../../common-installs-config.pri )

DATA_PATH = $${INSTALL_PREFIX}/share/libsignoncrypto-qt-tests/

testsuite.path = $$DATA_PATH
testsuite.files = tests.xml
INSTALLS += testsuite
